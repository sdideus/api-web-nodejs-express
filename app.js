const express = require("express");
const app = express();
const port = 3000;

app.get("/", (req, res) => res.send("API Web NodeJS + Express - GPTI - Tópicos Avançados - DevOps!"));

app.get("/users", (req, res) => {
  res.json([
    {
      id: 1,
      name: "Bruno Andrade",
    },
    {
      id: 2,
      name: "Johny Alves",
    },
    {
      id: 3,
      name: "Joao",
    },
        {
      id: 4,
      name: "Gabriel Leone",
    }, 
    {
      id: 5,
      name: "Marcelo Graciano",
    },        
    {
      id: 6,
      name: "Paulo",
    },
    {
      id: 7,
      name: "Yan Porfirio",
    }    
  ]);
});

app.get("/products", (req, res) => {
  res.json([
    {
      id: 1,
      name: "Azeitona",
    },
    {
      id: 2,
      name: "Molho de Tomate",
    } ,
    {
      id: 3,
      name: "Mussarela",
    }, 
    {
      id: 4,
      name: "Calabresa",
    }           
  ]);
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
